package monashuniversity.pulsemonitoring.model.dataclass;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bryanyhy on 1/6/2017.
 */

public class PairedBluetoothDevice implements Parcelable {

    private String name;
    private String macAddress;
    private String uuid;

    public PairedBluetoothDevice(String name, String macAddress) {
        this.name = name;
        this.macAddress = macAddress;
    }

    public String getName() {
        return name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    protected PairedBluetoothDevice(Parcel in) {
        name = in.readString();
        macAddress = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(macAddress);
    }

    @SuppressWarnings("unused")
    public static final Creator<PairedBluetoothDevice> CREATOR = new Creator<PairedBluetoothDevice>() {
        @Override
        public PairedBluetoothDevice createFromParcel(Parcel in) {
            return new PairedBluetoothDevice(in);
        }

        @Override
        public PairedBluetoothDevice[] newArray(int size) {
            return new PairedBluetoothDevice[size];
        }
    };
}