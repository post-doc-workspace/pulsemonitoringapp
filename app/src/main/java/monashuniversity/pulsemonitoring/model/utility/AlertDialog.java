package monashuniversity.pulsemonitoring.model.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Bryanyhy on 1/6/2017.
 */

public class AlertDialog {

    // show the alertBox with activity
    public static void showAlertBox(String title, String message, Activity activity) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(activity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    // show the alertBox with context
    public static void showAlertBoxThroughContext(String title, String message, Context context) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
