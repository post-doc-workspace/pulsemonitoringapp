package monashuniversity.pulsemonitoring;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import monashuniversity.pulsemonitoring.model.dataclass.DataFrame;
import monashuniversity.pulsemonitoring.model.dataclass.DataPacket;

import static monashuniversity.pulsemonitoring.model.utility.AlertDialog.showAlertBox;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSocket mBtSocket = null;
    private BluetoothDevice mBtDevice = null;
    private InputStream mInStream = null;
    private OutputStream mOutStream = null;
    private boolean connecting = false;
    private boolean dataTransmitting = false;
    private List<String[]> dataListToSave = new ArrayList<>();
    private String startRecordDateTime = null;
    private String [] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN};

    private Button startBtn, stopBtn;
    private Chronometer recordCm;

    private static final int REQUEST_PERMISSION = 0;
    private static final String DEVICE_MAC_ADDRESS = "00:1C:05:01:69:F6";
    private static final String SAVE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/OximeterRecordingStorage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!isDeviceSupportBluetooth()) {
            // if not, notify the user and close the application
            showAlertBox("Not Compatible", "Your phone does not support Bluetooth", this);
            finish();
        }
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION);
        turnBluetoothOn();
        startBtn = (Button) findViewById(R.id.startBtn);
        stopBtn = (Button) findViewById(R.id.stopBtn);
        recordCm = (Chronometer) findViewById(R.id.recordCm);
        // check if the folder for data storage exists
        File folder = new File(SAVE_PATH);
        if (!folder.exists()) {
            // if not, create folder
            folder.mkdir();
            Log.i("Folder Creation", "Oximeter folder is created");
        }
    }

    public void startRecording(View v) {
        if (!connecting) {
            if (isConnectToDeviceSuccessful()) {
                // TODO: 20/3/2018
                dataTransmitting = true;
                startRecordDateTime = new SimpleDateFormat("yyyyMMdd_HH:mm:ss").format(new Date());
                dataListToSave.clear();
                dataListToSave.add(new String[] {"Timestamp", "Time", "Sample", "SpO2", "HR"});
                Toast.makeText(this, "Recording is started", Toast.LENGTH_LONG).show();
                recordCm.setBase(SystemClock.elapsedRealtime());
                recordCm.start();
                new Thread(new ConnectedThread(mBtSocket)).start();
            } else {
                showAlertBox("Error", "Unable to connect to the Oximeter", this);
            }
        } else {
            showAlertBox("Reminder", "Recording is ongoing", this);
        }
    }

    public void stopRecording(View v) {
        if (connecting) {
            recordCm.stop();
            disconnectFromDevice();
            connecting = false;
            dataTransmitting = false;
            recordDataIntoCSV();
            Toast.makeText(this, "Recording is saved successfully", Toast.LENGTH_LONG).show();
        } else {
            showAlertBox("Reminder", "No recording is ongoing", this);
        }
    }

    public boolean isConnectToDeviceSuccessful()  {
        if (isBtAddressOnThePairList()) {
            ParcelUuid[] uuids = mBtDevice.getUuids();
            try {
                mBtSocket = mBtDevice.createInsecureRfcommSocketToServiceRecord(uuids[0].getUuid());
            } catch (IOException e) {
                Log.e("BtSocket Error", "Socket's create() method failed", e);
                return false;
            }
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();
            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mBtSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
//                    showAlertBox("Error", "Unable to connect to the Oximeter", this);
                    mBtSocket.close();
                } catch (IOException closeException) {
                    Log.e("Connection Error", "Could not close the client socket", closeException);
                }
                return false;
            }
            // The connection attempt succeeded.
            Log.i("Device Connection", "Device is connected");
            connecting = true;
            return true;
        } else {
            showAlertBox("Error", "Please pair up your device first.", this);
            return false;
        }
    }

    public void disconnectFromDevice() {
        try {
            if (mInStream != null) {
                mInStream.close();
            }
            if (mOutStream != null) {
                mOutStream.close();
            }
            if (mBtSocket != null) {
                mBtSocket.close();
            }
            Log.i("Device Disconnection", "Device is disconnected");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isDeviceSupportBluetooth() {
        if (mBluetoothAdapter == null) {
            return false;
        } else {
            return true;
        }
    }

    public void turnBluetoothOn() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
        }
    }

    public boolean isBtAddressOnThePairList() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                Log.i("MAC", device.getAddress());
                if (DEVICE_MAC_ADDRESS.equalsIgnoreCase(device.getAddress())) {
                    mBtDevice = mBluetoothAdapter.getRemoteDevice(DEVICE_MAC_ADDRESS);
                    return true;
                }
            }
        }
        return false;
    }

    // TODO: 26/3/2018
    /**
     * This method is used to set the data mode of the device
     *
     * @param dataMode the datamode to set on the device
     * @return
     */
    private void setDataMode(String dataMode) throws IOException {

        // Initialise the "init" packet
        byte initPacket[] = new byte[6];
        initPacket[0] = 0x02; // START
        initPacket[1] = 0x70; // Op Code
        initPacket[2] = 0x02; // Data Size
        initPacket[3] = 0x02; // Data Type

        if (dataMode.equalsIgnoreCase("D7")) {
            initPacket[4] = 0x07;
        } else if (dataMode.equalsIgnoreCase("D13")) {
            initPacket[4] = 0x0D;
        } else if (dataMode.equalsIgnoreCase("D8")) {
            initPacket[4] = 0x08;
        } else if (dataMode.equalsIgnoreCase("D2")) {
            initPacket[4] = 0x02;
        }

        initPacket[5] = 0x03; // ETX

//        // Initialise the "init" packet
//        byte initPacket[] = new byte[8];
//        initPacket[0] = 0x02; // START
//        initPacket[1] = 0x70; // Op Code
//        initPacket[2] = 0x04; // Data Size
//        initPacket[3] = 0x02; // Data Type
//
//        if (dataMode.equalsIgnoreCase("D7")) {
//            initPacket[4] = 0x07;
//        } else if (dataMode.equalsIgnoreCase("D13")) {
//            initPacket[4] = 0x0D;
//        } else if (dataMode.equalsIgnoreCase("D8")) {
//            initPacket[4] = 0x08;
//        } else if (dataMode.equalsIgnoreCase("D2")) {
//            initPacket[4] = 0x02;
//        }
//
//        initPacket[5] = 0x61; // Options, with all default setting
//        initPacket[6] = 0x1F; // checksum
//        initPacket[7] = 0x03; // ETX

        mOutStream.write(initPacket);
        Log.i("Set Data Mode", "Sent " + dataMode + " to the device");
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
//        private final InputStream mmInStream;
//        private final OutputStream mmOutStream;
        private byte[] mBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
                mInStream = tmpIn;
            } catch (IOException e) {
                Log.e("Input stream Error", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
                mOutStream = tmpOut;
                setDataMode("D7");
            } catch (IOException e) {
                Log.e("Output stream Error", "Error occurred when creating output stream", e);
            }
        }

        public void run() {
            if (!mInStream.equals(null)) {
                int FRAME_SIZE = 5;
                //circular buffer of bytes
                byte[] buffer = new byte[FRAME_SIZE];
                //position used in the buffer
                int buffPos = 0;
                DataPacket packet = new DataPacket();
                BufferedInputStream bufferIS = new BufferedInputStream(mInStream);


                while (dataTransmitting) {
                    if(buffPos >= FRAME_SIZE) {
                        //move bytes backwards
                        for (int k = 0; k < FRAME_SIZE - 1; k++) {
                            buffer[k] = buffer[k+1];
                        }
                        buffPos--;
                    }
                    try {
                        buffer[buffPos] = (byte) (bufferIS.read() & 0xFF);
                        if (buffer[buffPos] == 0x06) {
                            Log.i("Read response", "ACK");
                        } else if (buffer[buffPos] == 0x15) {
                            Log.i("Read response", "NAK");
                        }
                        buffPos++;
                    } catch (IOException ex) {
                        //probably the connection was closed, nothing to worry about
                        Log.e("Read buffer", "IOException");
                    }

                    if (DataFrame.IsValidFrame(buffer)) {
                        // Process this packet
                        if (DataFrame.IsSyncFrame(buffer)) {
                            // This packet must always be the first in the packet
                            //Log.d(Nonin.class.getName(),"Got sync frame");
                            packet = new DataPacket();
                        }
                        if (packet != null) {
                            //Log.d(Nonin.class.getName(),"Got a frame");
                            packet.addFrame(new DataFrame(buffer));

                            if (packet.isFull()) {
                                //Log.d(Nonin.class.getName(),"Got full packet spo2:"+packet.getDisplayedSpO2Average()+" hr:"+packet.getDisplayedHRAverage()+" artifacts: "+packet.hasAnyArtifact());
                                // Received a complete packet
                                // send it and start a new packet
                                storeDataFromPacket(packet);
                                packet = null;
                            }
                        }
                    }
                }
            }
        }

//        public void run() {
//            mBuffer = new byte[1024];
//            int numBytes; // bytes returned from read()
//
//            // Keep listening to the InputStream until an exception occurs.
//            while (true) {
//                try {
//                    // Read from the InputStream.
//                    numBytes = mInStream.read(mmBuffer);
//                    // Send the obtained bytes to the UI activity.
//                    Message readMsg = mHandler.obtainMessage(
//                            MessageConstants.MESSAGE_READ, numBytes, -1,
//                            mBuffer);
//                    readMsg.sendToTarget();
//                } catch (IOException e) {
//                    Log.d(TAG, "Input stream was disconnected", e);
//                    break;
//                }
//            }
//        }

//        // Call this from the main activity to send data to the remote device.
//        public void write(byte[] bytes) {
//            try {
//                mmOutStream.write(bytes);
//
//                // Share the sent message with the UI activity.
//                Message writtenMsg = mHandler.obtainMessage(
//                        MessageConstants.MESSAGE_WRITE, -1, -1, mmBuffer);
//                writtenMsg.sendToTarget();
//            } catch (IOException e) {
//                Log.e(TAG, "Error occurred when sending data", e);
//
//                // Send a failure message back to the activity.
//                Message writeErrorMsg =
//                        mHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
//                Bundle bundle = new Bundle();
//                bundle.putString("toast",
//                        "Couldn't send data to the other device");
//                writeErrorMsg.setData(bundle);
//                mHandler.sendMessage(writeErrorMsg);
//            }
//        }

    }

    public void storeDataFromPacket(DataPacket packet) {
        Log.i("DataPacket", "Get data from packet");
        String timestamp = Long.toString(System.currentTimeMillis());
        String time = Integer.toString(packet.getTimer());
        String spo2 = Integer.toString(packet.getSpO2Average());
        String hr = Integer.toString(packet.getHRAverage());
        String plethSample = null;
        for(int sample : packet.getPlethSamples()) {
            plethSample = Integer.toString(sample);
            dataListToSave.add(new String[] {timestamp, time, plethSample, spo2, hr});
        }
    }

    // record Shimmer data into device's drive
    public void recordDataIntoCSV() {
        //todo change file name with time
        String csv1 = SAVE_PATH + "/" + startRecordDateTime + ".csv";
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(csv1));
            writer.writeAll(dataListToSave);
            writer.close();
            Log.i("CSV1 save complete", "CSV1 success");
        } catch (IOException e) {
            Log.e("CSV save fail", "CSV error");
            showAlertBox("Alert", "Error occurred while saving data into CSV format", this);
        }
    }


}
